package fbeneditovm.DBUtils;

import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class Main {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "org.postgresql.Driver";
    static final String DB_URL = "jdbc:postgresql://localhost:5432/Test";

    //  Database credentials
    static final String USER = "postgres";
    static final String PASS = "";

    public static void main(String[] args) throws SQLException {
        Connection conn = null;
        QueryRunner queryRunner = new QueryRunner();

        //Step 1: Register JDBC driver
        DbUtils.loadDriver(JDBC_DRIVER);

        //Step 2: Open a connection
        System.out.println("Connecting to database...");
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
        System.out.println(UUID.randomUUID().toString().length());
        try {
            /* ---- Select Single Value ---- */
            //Create a ResultSet Handler to handle Employee Beans
            ResultSetHandler<Employee> resultHandler = new BeanHandler<Employee>(Employee.class);
            Employee emp = queryRunner.query(conn, "SELECT * FROM employees WHERE first=?",
                    resultHandler, "Sumit");
            //Display values
            System.out.print("ID: " + emp.getId());
            System.out.print(", Age: " + emp.getAge());
            System.out.print(", First: " + emp.getFirst());
            System.out.println(", Last: " + emp.getLast());

            /* ---- Select Multiple Values ---- */
            ResultSetHandler<List<Employee>> resultListHandler = new BeanListHandler<Employee>(Employee.class);
            List<Employee> empList = queryRunner.query(conn, "SELECT * from employees", resultListHandler);
            for(Employee empIterator : empList){
                //Display values
                System.out.print("ID: " + empIterator.getId());
                System.out.print(", Age: " + empIterator.getAge());
                System.out.print(", First: " + empIterator.getFirst());
                System.out.println(", Last: " + empIterator.getLast());
            }

            /* ---- Update ---- */
            int deletedRecords = queryRunner.update(conn, "DELETE from employees WHERE id=?", 104);
            System.out.println(deletedRecords + " records deleted.");

            /* ---- Insert ---- */
            int insertedRecords = queryRunner.update(conn,
                    "INSERT INTO employees(id,age,first,last)  VALUES (?,?,?,?)",
                    104,30, "Sohan","Kumar");
            System.out.println(insertedRecords + " records inserted.");

            /* ---- Update ---- */
            int updatedRecords = queryRunner.update(conn, "UPDATE employees SET age=? WHERE id=?", 33, 104);
            System.out.println(updatedRecords + " records updated.");


            /* ---- Create your personal ResultSetHandler ---- */
            ResultSetHandler<Object[]> handler = new ResultSetHandler<Object[]>() {
                public Object[] handle(ResultSet rs) throws SQLException {
                    if (!rs.next()) {
                        return null;
                    }
                    ResultSetMetaData meta = rs.getMetaData();
                    int cols = meta.getColumnCount();
                    Object[] result = new Object[cols];

                    for (int i = 0; i < cols; i++) {
                        result[i] = rs.getObject(i + 1);
                    }
                    return result;
                }
            };
            Object[] result  = queryRunner.query(conn, "SELECT * FROM employees WHERE id=?",
                    handler, 103);
            //Display values
            System.out.print("Result: " + Arrays.toString(result));


        } finally {
            DbUtils.close(conn);
        }
    }
}